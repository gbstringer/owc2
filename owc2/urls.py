"""owc2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views

from owc.views import BookList, BookAdd, BookDetail, BookEdit, BookDelete, \
    BookPersonAdd, \
    PersonList, PersonDetail, PersonAdd, PersonEdit, \
    EditionList, EditionDetail, EditionAdd, EditionEdit, EditionDelete, \
    InstanceList, InstanceAdd, InstanceEdit, InstanceDetail, InstanceDelete

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', TemplateView.as_view(template_name='about.html'), name='about'),

    path('books/', BookList.as_view(), name='books'),
    path('book/add/', BookAdd.as_view(), name='bookadd'),
    path('book/<int:pk>/', BookDetail.as_view(), name='bookdetail'),
    path('book/edit/<int:pk>/', BookEdit.as_view(), name='bookedit'),
    path('book/delete/<int:pk>/',BookDelete.as_view(), name='bookdelete'),

    path('bookperson/add/', BookPersonAdd.as_view(), name='bookpersonadd'),

    path('people/', PersonList.as_view(), name='people'),
    path('person/<int:pk>/', PersonDetail.as_view(), name='persondetail'),
    path('person/new', PersonAdd.as_view(), name='personadd'),
    path('person/edit/<int:pk>/', PersonEdit.as_view(), name='personedit'),

    path('editions/',EditionList.as_view(), name='editions'),
    path('edition/<int:pk>/', EditionDetail.as_view(), name='editiondetail'),
    path('edition/add/', EditionAdd.as_view(), name='editionadd'),
    path('edition/edit/<int:pk>/', EditionEdit.as_view(), name='editionedit'),
    path('edition/delete/<int:pk>/', EditionDelete.as_view(), name='editiondelete'),

    path('mybooks/',InstanceList.as_view(), name='instances'),
    path('mybooks/add/', InstanceAdd.as_view(), name='instanceadd'),
    path('mybooks/<int:pk>/', InstanceDetail.as_view(), name='instancedetail'),
    path('mybooks/edit/<int:pk>/', InstanceEdit.as_view(), name='instanceedit'),
    path('mybooks/delete/<int:pk>/', InstanceDelete.as_view(), name='instancedelete'),


#   path('accounts/', include('django.contrib.auth.urls')),
    path('login/', auth_views.LoginView.as_view(template_name='auth/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='auth/logged_out.html'), name='logout'),
    path('accounts/profile/', auth_views.TemplateView.as_view(template_name='auth/details.html'), name='profile'),
 #   path('accounts/password_reset/', auth_views.TemplateView.as_view(template_name='auth/password_reset.html'), name='password_reset'),
 #   path('accounts/password_change/', auth_views.TemplateView.as_view(template_name='auth/password_change.html'),
 #        name='password_change'),

    path('search/', include('haystack.urls')),

]
