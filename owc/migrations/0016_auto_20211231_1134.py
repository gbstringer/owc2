# Generated by Django 3.1.2 on 2021-12-31 11:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('owc', '0015_djtype_is_illustrated'),
    ]

    operations = [
        migrations.CreateModel(
            name='DJPerson',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('role', models.CharField(choices=[('author', 'Author'), ('contributor', 'Contributor'), ('editor', 'Editor'), ('translator', 'Translator')], max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='DustJacket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.AlterModelOptions(
            name='djtype',
            options={'verbose_name': 'Dustjacket Type', 'verbose_name_plural': 'Dustjacket Types'},
        ),
        migrations.DeleteModel(
            name='EditionPerson',
        ),
        migrations.AddField(
            model_name='dustjacket',
            name='type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='owc.djtype'),
        ),
        migrations.AddField(
            model_name='djperson',
            name='dustjacket',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='owc.dustjacket'),
        ),
        migrations.AddField(
            model_name='djperson',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='owc.person'),
        ),
    ]
