# Generated by Django 3.1.2 on 2021-12-31 11:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('owc', '0017_auto_20211231_1152'),
    ]

    operations = [
        migrations.AddField(
            model_name='dustjacket',
            name='edition',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='owc.edition'),
        ),
        migrations.AddField(
            model_name='instance',
            name='has_dj',
            field=models.BooleanField(default=False),
        ),
    ]
