from django.contrib import admin

from .models import Book, Person, BookPerson, Edition, Instance, \
    DJPerson, DJType, DustJacket

# Register your models here.

admin.site.register(Book)
admin.site.register(Person)
admin.site.register(BookPerson)
admin.site.register(Edition)
admin.site.register(Instance)
admin.site.register(DustJacket)
admin.site.register(DJPerson)
admin.site.register(DJType)


