import datetime
from haystack import indexes
#from haystack import site
from owc.models import Book



class BookIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
#    subtitle = indexes.CharField(model_attr='subtitle')

    def get_model(self):
        return Book

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

