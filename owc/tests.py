from django.test import TestCase

from .models import Book
# Create your tests here.

class DatabaseTestCase(TestCase):
    def setUp(self):
        Book.objects.create(user_id=1, name='test1')

    def test_database_item_created(self):
        mytest1 = Book.objects.get(name='test1')
        self.assertEqual(mytest1.name, 'test1', 'Unable to create basic Book record in database.')