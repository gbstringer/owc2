from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.urls.base import reverse, reverse_lazy
from django.db.models import Sum
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin

from owc.models import Book, BookPerson, Person, Edition, Instance


class BookList(ListView):
    model = Book
    context_object_name='books'
    template_name = 'book/list.html'
    paginate_by = 20


class BookAdd(LoginRequiredMixin, CreateView):
    model = Book
    fields = ['title', 'subtitle', 'pubyear', 'volume', 'user']
    template_name = 'book/add.html'
    success_url = reverse_lazy('books')


class BookDetail(DetailView):
    model = Book
    template_name = 'book/detail.html'


class BookEdit(LoginRequiredMixin, UpdateView):
    model = Book
    fields = ['title', 'subtitle', 'pubyear', 'volume', 'user']
    context_object_name = 'book'
    template_name = 'book/edit.html'

    def get_success_url(self,**kwargs):
        return self.object.get_absolute_url()


class BookDelete(LoginRequiredMixin, DeleteView):
    model = Book
    context_object_name = 'book'
    template_name = 'book/confirm_delete.html'
    success_url = reverse_lazy('books')


class BookPersonAdd(LoginRequiredMixin, CreateView):
    model = BookPerson
    fields = ['role', 'person', 'book']
    template_name = 'bookperson/add.html'


class PersonList(ListView):
    model = Person
    context_object_name = 'people'
    template_name = 'person/list.html'
    paginate_by = 10


class PersonDetail(DetailView):
    model = Person
    context_object_name = 'person'
    template_name = 'person/detail.html'


class PersonAdd(LoginRequiredMixin, CreateView):
    model = Person
    fields = ['firstname', 'lastname', 'yearborn', 'yeardied', 'viaf']
    template_name = 'person/add.html'
    success_url = reverse_lazy('people')


class PersonEdit(LoginRequiredMixin, UpdateView):
    model = Person
    fields = ['firstname', 'lastname', 'yearborn', 'yeardied', 'viaf']
    template_name = 'person/edit.html'
    success_url = reverse_lazy('people')


class EditionList(ListView):
    model=Edition
    context_object_name='editions'
    template_name = 'edition/list.html'
    paginate_by = 20


class EditionDetail(DetailView):
    model = Edition
    template_name = 'edition/detail.html'


class EditionAdd(LoginRequiredMixin, CreateView):
    model = Edition
    fields = ['year', 'book', 'pagination', 'description', 'contents']
    template_name = 'edition/add.html'
    success_url = reverse_lazy('editions')


class EditionEdit(LoginRequiredMixin,  UpdateView):
    model = Edition
    fields = ['year', 'book', 'pagination', 'description', 'contents']
    template_name = 'edition/edit.html'
    success_url = reverse_lazy('editions')

class EditionDelete(LoginRequiredMixin, DeleteView):
    model = Edition
    template_name = 'edition/delete.html'
    success_url = reverse_lazy('editions')

class InstanceList(ListView):
    model = Instance
    context_object_name = 'instances'
    template_name = 'instance/list.html'

    def totalcost(self):
        total = Instance.objects.aggregate(Sum('price'))
        totalstring = '{0:.2f}'.format(total['price__sum'])
        return totalstring


class InstanceAdd(LoginRequiredMixin, CreateView):
    model = Instance
    fields = ['edition','condition','price']
    template_name = 'instance/add.html'
    success_url = reverse_lazy('instances')


class InstanceEdit(LoginRequiredMixin, UpdateView):
    model = Instance
    fields = ['edition','condition','price']
    template_name = 'instance/edit.html'
    success_url = reverse_lazy('instances')


class InstanceDetail(DetailView):
    model = Instance
    context_object_name = 'instance'
    template_name = 'instance/detail.html'


class InstanceDelete(DeleteView):
    model = Instance
    context_object_name = 'instance'
    template_name = 'instance/confirm_delete.html'
    success_url = reverse_lazy('instances')


class MyInstanceList(ListView):
    model = Instance

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        #context['totalcost'] = Instance.objects.aggregate(context,sum('price'))
        return context

