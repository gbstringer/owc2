from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse


ROLECHOICE = (
    ('author','Author'),
    ('contributor','Contributor'),
    ('editor','Editor'),
    ('translator', 'Translator'),
    ('illustrator', 'Illustrator'),
    ('subject', 'Depicted Subject'),
    )


class Book(models.Model):
    title = models.CharField(max_length=64)
    subtitle = models.CharField(max_length=255, blank=True, null=True)
    user = models.ForeignKey(User, models.DO_NOTHING, null=True)
    volume = models.IntegerField(null=True)
    pubyear = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return str(self.volume) + ': ' + self.title

    def get_absolute_url(self):
        return reverse('bookdetail', kwargs={'pk': self.pk})

    def get_personlist(self):
        personlist = self.bookperson_set.all()
        result = []
        for a, p in enumerate(personlist):

            result += [[a, p.person.firstname + ' ' + p.person.lastname, p.person.id, p.role]]
        return result

    def get_personstring(self):
        plist = self.bookperson_set.all()
        result = ""
        for i, p in enumerate(plist):
            if i: result += ', '
            result += p.person.firstname + ' ' + p.person.lastname + ' (' + p.role +')'
        return result

    def get_editionlist(self):
        editionlist = self.edition_set.all()
        return editionlist

    def get_dustjacketlist(self):
        dustjacketlist = self.edition_set.all()
        return dustjacketlist


    class Meta:
        ordering = ['volume']


class Person(models.Model):
    firstname = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200)
    yearborn = models.IntegerField(null=True, blank=True)
    yeardied = models.IntegerField(null=True, blank=True)
    viaf = models.CharField(max_length=32, blank=True)

    def __str__(self):
        return self.lastname + ', ' + self.firstname

    class Meta:
        ordering = ['lastname', 'firstname']
        verbose_name_plural = 'People'

    def get_editioncount(self):
        vols = BookPerson.objects.get(book=self.id)
        return vols

    def get_booklist(self):
        booklist = self.bookperson_set.all().order_by('book')
        return booklist

#    get_dustjacketlist()
#    @todo: needs a route from book to dustjacket
    def get_dustjacketlist(self):
        dustjacketlist = self.djperson_set.all()
        return dustjacketlist


class BookPerson(models.Model):
    role = models.CharField(max_length=32, choices=ROLECHOICE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)

    def __str__(self):
        #numworks = self.objects.get(role='author')count()
        numworks = 0
        return self.person.lastname + ', ' + self.person.firstname + ' was ' + self.role + ' of ' + self.book.title
        #return str(numworks)

    class Meta:
        verbose_name_plural = 'Books/People'
        verbose_name = 'Book/Person'

    def get_short_role(self):
        match self.role:
            case 'author': return ""
            case 'translator': return "(trans.)"
            case 'editor': return "(ed.)"
            case 'contributor': "(contrib.)"
            case _: return "(unknown"

class DJType(models.Model):
    is_illustrated = models.BooleanField(default=False)
    generic_desc = models.CharField(max_length=255, blank=True)

    def __str__(self):
    #    return self.generic_desc+" ("+ ("individually illustrated" if self.is_illustrated else "generic design")+")"
        return self.generic_desc
    class Meta:
        verbose_name = 'Dustjacket Type'
        verbose_name_plural = 'Dustjacket Types'

class Edition(models.Model):
    year = models.IntegerField(null=True)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    pagination = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    contents = models.TextField(null=True, blank=True)
    dj_type = models.ForeignKey(DJType, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.book.__str__() + ' (' + str(self.year) + ')'

    class Meta:
        ordering = ['book__volume', 'year']


class Instance(models.Model):
    condition = models.CharField(max_length=64)
    price = models.FloatField(default=0.00)
    has_dj = models.BooleanField(default=False)
    edition = models.ForeignKey(Edition, on_delete=models.CASCADE)
    user = models.ForeignKey(User, models.DO_NOTHING, null=True)

    def __str__(self):
        return self.edition.__str__() + ' (' + self.condition + ', £' + self.get_coststring() + ')'

    def get_coststring(self):
        coststring = '{0:.2f}'.format(self.price)
        return coststring

    def get_creatorlist(self):
        creatorlist = self.edition.book.bookperson_set.all()
        return creatorlist

    def get_has_dj(self):
        dustjacket = "yes" if self.has_dj else "no"
        return dustjacket



    class Meta:
        ordering = ['edition__book__volume', 'edition__year']



class DustJacket(models.Model):
    edition = models.ForeignKey(Edition, on_delete=models.CASCADE, null=True)
    type = models.ForeignKey(DJType, on_delete=models.CASCADE, null=True)
    illustration = models.TextField(blank=True)
    colour = models.CharField(max_length=255, blank=True)
    features = models.TextField(blank=True)

    def __str__(self):
        return self.edition.__str__() + " ("+self.type.__str__()+")"


class DJPerson(models.Model):
    role = models.CharField(max_length=32, choices=ROLECHOICE)
    dustjacket = models.ForeignKey(DustJacket, on_delete=models.CASCADE, null=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)

    def __str__(self):
        return "" + self.person.__str__() + " is "+self.role+" for "+self.dustjacket.__str__()


